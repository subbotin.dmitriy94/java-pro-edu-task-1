package ru.vtb.study.java.pro.logic.service;

import java.util.List;

/**
 * UserService.
 *
 * @author Dmitriy Subbotin
 */
public interface UserService<T> {

  void save(String login, String fullNameRus);

  void delete(String login);

  T getUser(String login);

  List<T> getAll();
}
