package ru.vtb.study.java.pro.test.exception;

/**
 * TestAnnotationException.
 *
 * @author Dmitriy Subbotin
 */
public class TestAnnotationException extends RuntimeException {

  public TestAnnotationException(String message) {
    super(message);
  }

  public TestAnnotationException(Throwable cause) {
    super(cause);
  }
}
