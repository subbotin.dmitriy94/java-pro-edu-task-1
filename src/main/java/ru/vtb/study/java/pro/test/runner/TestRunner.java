package ru.vtb.study.java.pro.test.runner;

import ru.vtb.study.java.pro.test.annotation.AfterSuite;
import ru.vtb.study.java.pro.test.annotation.BeforeSuite;
import ru.vtb.study.java.pro.test.annotation.CsvSource;
import ru.vtb.study.java.pro.test.annotation.Test;
import ru.vtb.study.java.pro.test.exception.TestAnnotationException;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;

/**
 * TestRunner.
 *
 * @author Dmitriy Subbotin
 */
public class TestRunner {

  public static void runTests(Class<?> aClass) {
    Method[] methods = aClass.getDeclaredMethods();

    checkCorrectAnnotations(methods);

    invokeMethod(AfterSuite.class, aClass, methods);
    invokeMethod(Test.class, aClass, methods);
    invokeMethod(BeforeSuite.class, aClass, methods);

    Arrays.stream(methods)
        .filter(method -> method.isAnnotationPresent(CsvSource.class))
        .forEach(method -> invokeCsvSourceMethod(aClass, method));

    System.out.println("runTests success");
  }

  private static void invokeMethod(Class<? extends Annotation> annotation, Class<?> testClass,
      Method[] methods) {
    Arrays.stream(methods)
        .filter(method -> method.isAnnotationPresent(annotation))
        .forEach(method -> {
          try {
            System.out.printf("Invoke method: %s, annotation: @%s %n", method.getName(),
                annotation.getSimpleName());
            if ("Test".equals(annotation.getSimpleName())) {
              System.out.printf("Priority = %d%n", method.getAnnotation(Test.class).priority());
            }
            method.setAccessible(true);
            if (Modifier.isStatic(method.getModifiers())) {
              method.invoke(testClass);
            } else {
              method.invoke(getInstance(testClass));
            }
          } catch (IllegalAccessException | InvocationTargetException e) {
            throw new TestAnnotationException(e);
          }
        });
  }

  private static Object getInstance(Class<?> aClass) {
    try {
      return aClass.getDeclaredConstructor().newInstance();
    } catch (InstantiationException | IllegalAccessException | InvocationTargetException |
             NoSuchMethodException e) {
      throw new TestAnnotationException(e);
    }
  }

  private static void invokeCsvSourceMethod(Class<?> aClass, Method method) {
    CsvSource csvSource = method.getAnnotation(CsvSource.class);
    String csvString = csvSource.value();

    String[] argumentsValue =
        Arrays.stream(csvString.split(","))
            .map(String::trim)
            .toArray(String[]::new);

    Class<?>[] methodParameterTypes = method.getParameterTypes();

    if (argumentsValue.length != methodParameterTypes.length) {
      throw new TestAnnotationException(
          "Parameters must be equals in string value for @CsvSource, but args = "
              + argumentsValue.length + " and params = " + methodParameterTypes.length);
    }

    Object[] args = new Object[argumentsValue.length];

    for (int i = 0; i < args.length; i++) {
      if ("int".equals(methodParameterTypes[i].getName())) {
        args[i] = Integer.parseInt(argumentsValue[i]);
      } else if ("java.lang.String".equals(methodParameterTypes[i].getName())) {
        args[i] = argumentsValue[i];
      } else if ("boolean".equals(methodParameterTypes[i].getName())) {
        args[i] = Boolean.parseBoolean(argumentsValue[i]);
      }
    }

    try {
      method.invoke(aClass, args);
    } catch (IllegalAccessException | InvocationTargetException e) {
      throw new TestAnnotationException(e);
    }
  }

  private static void checkCorrectAnnotations(Method[] methods) {
    int afterSuiteCount = 0;
    int beforeSuiteCount = 0;

    for (Method method : methods) {

      if (method.isAnnotationPresent(Test.class)) {
        Test test = method.getAnnotation(Test.class);
        if (test.priority() < 1 || test.priority() > 10) {
          throw new TestAnnotationException(
              String.format("Priority mustn't be %d, only from 1 to 10", test.priority()));
        }
      }

      if ((method.isAnnotationPresent(AfterSuite.class) ||
          method.isAnnotationPresent(BeforeSuite.class)) &&
          !Modifier.isStatic(method.getModifiers())) {
        throw new TestAnnotationException(
            "Annotations AfterSuite and BeforeSuite only for static methods");
      }

      if (method.isAnnotationPresent(AfterSuite.class)) {
        afterSuiteCount++;
        if (afterSuiteCount > 1) {
          throw new TestAnnotationException("More than 1 static method with AfterSuite annotation");
        }
      }

      if (method.isAnnotationPresent(BeforeSuite.class)) {
        beforeSuiteCount++;
        if (beforeSuiteCount > 1) {
          throw new TestAnnotationException(
              "More than 1 static method with BeforeSuite annotation");
        }
      }
    }
  }
}
