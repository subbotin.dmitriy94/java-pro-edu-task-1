package ru.vtb.study.java.pro.logic.repository;

import ru.vtb.study.java.pro.logic.dto.User;

import java.util.ArrayList;
import java.util.List;

/**
 * UserRepositoryImpl.
 *
 * @author Dmitriy Subbotin
 */
public class UserRepositoryImpl implements UserRepository<User> {

  private static final UserRepository<User> INSTANCE = new UserRepositoryImpl();

  private final List<User> users = new ArrayList<>();

  private UserRepositoryImpl() {
  }

  public static UserRepository<User> getInstance() {
    return INSTANCE;
  }

  @Override
  public void save(User user) {
    users.add(user);
  }

  @Override
  public void delete(User user) {
    users.remove(user);
  }

  @Override
  public User get(String login) {
    return users.stream()
        .filter(u -> u.getLogin().equalsIgnoreCase(login))
        .findFirst()
        .orElse(null);
  }

  @Override
  public List<User> getAll() {
    return users;
  }
}
