package ru.vtb.study.java.pro.logic.dto;

import java.util.Objects;
import java.util.UUID;

/**
 * User.
 *
 * @author Dmitriy Subbotin
 */
public class User {

  private final UUID id;
  private final String login;
  private final String fullNameRus;

  public User(UUID id, String login, String fullNameRus) {
    this.id = id;
    this.login = login;
    this.fullNameRus = fullNameRus;
  }

  public UUID getId() {
    return id;
  }

  public String getLogin() {
    return login;
  }

  public String getFullNameRus() {
    return fullNameRus;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    User user = (User) o;
    return Objects.equals(id, user.id) && Objects.equals(login, user.login)
        && Objects.equals(fullNameRus, user.fullNameRus);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, login, fullNameRus);
  }

  @Override
  public String toString() {
    return "User{" +
        "id=" + id +
        ", login='" + login + '\'' +
        ", fullNameRus='" + fullNameRus + '\'' +
        '}';
  }
}
