package ru.vtb.study.java.pro.logic.service;

import ru.vtb.study.java.pro.logic.dto.User;
import ru.vtb.study.java.pro.logic.repository.UserRepository;
import ru.vtb.study.java.pro.logic.repository.UserRepositoryImpl;
import ru.vtb.study.java.pro.test.annotation.CsvSource;

import java.util.List;
import java.util.UUID;

/**
 * UserServiceImpl.
 *
 * @author Dmitriy Subbotin
 */
public class UserServiceImpl implements UserService<User> {

  private static final UserService<User> INSTANCE = new UserServiceImpl();

  private final UserRepository<User> userRepository = UserRepositoryImpl.getInstance();

  private UserServiceImpl() {
  }

  public static UserService<User> getInstance() {
    return INSTANCE;
  }

  @Override
  public void save(String login, String fullNameRus) {
    User user = new User(UUID.randomUUID(), login, fullNameRus);
    userRepository.save(user);
    System.out.printf("Пользователь - %s, сохранен %n", fullNameRus);
  }

  @Override
  public void delete(String login) {
    if (login == null || login.isEmpty()) {
      System.out.println("Login incorrect");
      return;
    }

    User user = userRepository.get(login);

    if (user == null) {
      System.out.println("User not found");
      return;
    }

    userRepository.delete(user);
    System.out.printf("Пользователь - %s, удален %n", user.getFullNameRus());
  }

  @Override
  public User getUser(String login) {

    User user = userRepository.get(login);

    if (user == null) {
      System.out.println("User '" + login + "' not found");
      return null;
    }

    System.out.printf("User '%s' found %n", user.getFullNameRus());
    return user;
  }

  @Override
  public List<User> getAll() {
    return userRepository.getAll();
  }

  @CsvSource("1, Developer,       10, true")
  public void parseCsvString(int number, String type, int count, boolean isNew) {
    System.out.println("String number: " + number);
    System.out.println("User type: " + type);
    System.out.println("User count: " + count);
    System.out.println("Users is new: " + isNew);
  }
}
