package ru.vtb.study.java.pro.logic.repository;

import java.util.List;

/**
 * UserRepository.
 *
 * @author Dmitriy Subbotin
 */
public interface UserRepository<T> {

  void save(T t);

  void delete(T t);

  T get(String login);

  List<T> getAll();
}
