package ru.vtb.study.java.pro;

import ru.vtb.study.java.pro.test.runner.TestRunner;
import ru.vtb.study.java.pro.test.service.UserServiceTest;

/**
 * Main.
 *
 * @author Dmitriy Subbotin
 */
public class Main {

  public static void main(String[] args) {
    TestRunner.runTests(UserServiceTest.class);
  }
}