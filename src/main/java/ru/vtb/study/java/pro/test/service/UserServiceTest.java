package ru.vtb.study.java.pro.test.service;

import ru.vtb.study.java.pro.logic.dto.User;
import ru.vtb.study.java.pro.logic.service.UserService;
import ru.vtb.study.java.pro.logic.service.UserServiceImpl;
import ru.vtb.study.java.pro.test.annotation.AfterSuite;
import ru.vtb.study.java.pro.test.annotation.BeforeSuite;
import ru.vtb.study.java.pro.test.annotation.CsvSource;
import ru.vtb.study.java.pro.test.annotation.Test;
import ru.vtb.study.java.pro.test.exception.TestAnnotationException;

/**
 * UserServiceTest.
 *
 * @author Dmitriy Subbotin (VTB4090513)
 */
public class UserServiceTest {

  private static UserService<User> userService;

  private final String login = "admin";
  private final String name = "Петров Петр";

  @AfterSuite
  static void afterSuite() {
    userService = UserServiceImpl.getInstance();
  }

  @BeforeSuite
  static void beforeSuite() {
    userService = null;
  }

  @Test(priority = 1)
  void saveTest() {
    userService.save(login, name);
    User user = userService.getUser(login);
    if (!user.getLogin().equals(login)) {
      throw new TestAnnotationException("Method 'save' doesn't correct work");
    }
  }

  @Test
  void deleteTest() {
    int startSize = userService.getAll().size();
    userService.save(login, name);
    int beforeSaveUserSize = userService.getAll().size();
    if (startSize + 1 != beforeSaveUserSize) {
      throw new TestAnnotationException("Method 'save' doesn't correct work");
    }
    userService.delete(login);
    int endSize = userService.getAll().size();

    if (startSize != endSize) {
      throw new TestAnnotationException("Method 'delete' doesn't correct work");
    }
  }

  @Test
  void getUserTest() {
    if (userService.getAll().isEmpty()) {
      userService.save(login, name);
    }

    User user = userService.getUser(login);

    if (!user.getLogin().equals(login)) {
      throw new TestAnnotationException("Method 'get' doesn't correct work");
    }
  }

  @Test(priority = 9)
  void getAllTest() {
    int startUserCount = userService.getAll().size();
    int addedCount = 2;
    for (int i = 0; i < addedCount; i++) {
      userService.save(login + i, name + i);
    }
    int endUserCount = userService.getAll().size();
    if (startUserCount + addedCount != endUserCount) {
      throw new TestAnnotationException("Method 'getAll' doesn't correct work");
    }
  }

  @CsvSource("1, Developer,       10, true")
  public void parseCsvString(int number, String type, int count, boolean isNew) {
    System.out.println("String number: " + number);
    System.out.println("User type: " + type);
    System.out.println("User count: " + count);
    System.out.println("Users is new: " + isNew);
  }
}
